import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { pluck } from "rxjs/operators";

@Component({
  selector: "app-message",
  templateUrl: "./message.component.html",
  styleUrls: ["./message.component.scss"]
})
export class MessageComponent implements OnInit {
  
  message$ = this.route.data.pipe(pluck("message"));

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {}
}
