import { Component, OnInit } from "@angular/core";
import { PlaylistsService } from "../../services/playlists.service";
import { ActivatedRoute, Router, Data } from "@angular/router";
import { map, switchMap, pluck, startWith, tap, concat } from "rxjs/operators";
import { Playlist } from "src/app/model/Playlist";
import { Observable } from "rxjs";

@Component({
  selector: "app-selected-playlist",
  template: `
    <app-playlist-details
      *ngIf="(playlist$ | async) as playlist"
      (playlistChange)="save($event)"
      [playlist]="playlist"
    >
    </app-playlist-details>
  `,
  styles: []
})
export class SelectedPlaylistComponent implements OnInit {
 
  playlist$ = this.service.selected;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {}

  save(playlist: Playlist) {
    this.service.save(playlist);
    
    this.service.selectPlaylist(playlist);
  }

  ngOnInit() {}
}
