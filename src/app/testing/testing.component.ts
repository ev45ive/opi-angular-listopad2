import { Component, OnInit } from '@angular/core';
import { TestingService } from '../testing.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit {

  message = 'testing works!'

  constructor(private service:TestingService) { }

  save(){
    return this.service.sendMessage(this.message)
  }

  ngOnInit() {
  }

}
