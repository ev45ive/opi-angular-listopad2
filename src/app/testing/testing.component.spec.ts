import { async, ComponentFixture, TestBed, inject } from "@angular/core/testing";

import { TestingComponent } from "./testing.component";
import { By } from "@angular/platform-browser";
import { FormsModule, NgModel } from "@angular/forms";
import { TestingService } from '../testing.service';
import { Subject } from 'rxjs';

fdescribe("TestingComponent", () => {
  let fixture: ComponentFixture<TestingComponent>;
  let instance: TestingComponent;
 
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      imports: [FormsModule],
      providers: [
        {
          provide: TestingService,
          // useClass: TestingService,
          useValue: jasmine.createSpyObj('MessageService',['sendMessage'])
        }
      ]
    });
    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    instance = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(instance).toBeTruthy();
  });

  it("should render message", () => {
    const elem = fixture.debugElement.query(By.css("p"));
    expect(elem.nativeElement.textContent).toMatch("testing works!");
  });

  it("should render changed message", () => {
    instance.message = "Placki!";
    fixture.detectChanges();

    const elem = fixture.debugElement.query(By.css("p"));
    expect(elem.nativeElement.textContent).toMatch("Placki!");
  });

  it("should render message in form input", () => {
    const elem = fixture.debugElement.query(By.css("input"));

    return fixture.whenStable().then(() => {
      expect(elem.nativeElement.value).toBe(instance.message);
    });
  });

  it("should update message when input value changes", () => {
    const elem = fixture.debugElement.query(By.directive(NgModel));
    // const model = elem.injector.get(NgModel)
    // debugger

    elem.nativeElement.value = "Changed!";
    // NativeEvent:
    // elem.nativeElement.dispatchEvent(new Event('input'))

    // MockEvent:
    elem.triggerEventHandler("input", {
      target: elem.nativeElement
    });

    expect(instance.message).toBe("Changed!");
  });

  it("should call save method when save button is clicked", () => {
    const elem = fixture.debugElement.query(By.css("[type=button],button"));

    const spy = spyOn(instance, "save");

    elem.triggerEventHandler("click", {});
    expect(spy).toHaveBeenCalledWith(instance.message);
  });
  
  it('should save message to MessageService',inject([TestingService],(service:jasmine.SpyObj<TestingService>)=>{

    // const spy = spyOn(service,'sendMessage')
    const obs = new Subject()
    service.sendMessage.and.returnValue(obs)
    
    instance.message = 'placki!'
    instance.save().subscribe((resp)=>{
      expect(resp).toEqual('response')
    })
    obs.next('response')
    
    expect(service.sendMessage).toHaveBeenCalledWith(instance.message);
  }))

});
