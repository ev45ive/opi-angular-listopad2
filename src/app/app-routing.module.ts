import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    // component: HomeComponent,
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path:'music',
    // children:[]
    loadChildren:'./music/music.module#MusicModule'
  },
  {
    path: "**",
    // component: PageNotFoundComponent,
    redirectTo: "playlists",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      onSameUrlNavigation: "reload"
      // useHash: true,
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
